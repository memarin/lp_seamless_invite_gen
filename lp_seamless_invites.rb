require 'http'
require 'faker'
require 'json'
require 'nokogiri'

class LPSeamlessInvites

  def initialize
    doc = Nokogiri::XML.parse(File.read('config.xml'))
    @user = doc.xpath("root/api_key").text
    @environment_url= doc.xpath("root/url").text
    @interview_id = doc.xpath("root/interview_id").text
  end

  def generate_candidate_invites(count)
    for num in 1..count.to_i do
      body = generate_candidate_seamless_invite
      puts num.to_s + ' - ' + body['response']['link']['url'] + ' - ' + body['response']['email']
    end
  end

  private

  def authorization
    HTTP.basic_auth(user: @user, pass: @pass)
  end

  def generate_candidate_details
    candidate = { :first_name => Faker::Name.first_name,
                  :last_name => Faker::Name.last_name }
    candidate[:email] = (candidate[:first_name] + candidate[:last_name] + '@mailinator.com').downcase
    return candidate
  end

  def create_candidate(candidate)
    authorization.post(@environment_url + 'candidates', form: {email: candidate[:email],
                                                first_name: candidate[:first_name],
                                                last_name: candidate[:last_name]})
  end

  def generate_seamless_invite(candidate_id)
    authorization.post(@environment_url + 'interviews/' + @interview_id + '/seamless_login_invite', form: {candidate_id: candidate_id})
  end

  def generate_candidate_seamless_invite
    body = JSON.parse(create_candidate(generate_candidate_details).body)
    JSON.parse(generate_seamless_invite(body['response']['candidate_id']).body)
  end

end

lp_candidate = LPSeamlessInvites.new
puts "How many seamless invites do you want?"
count = gets.chomp
lp_candidate.generate_candidate_invites(count)
puts '=================END================='
